<?php

use Illuminate\Database\Seeder;

class DemoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('demos')->insert([
        	'name' => 'dharshan',
        	'email' => 'dharshan@gmail.com',
        	'password' => \Hash::make('123456'),
        ]);
    }
}
