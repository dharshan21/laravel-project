<?php

namespace App;

use App\Http\Controllers\HomeController;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Moblies;
class Moblies extends Model
{
    protected $fillable = [

        'name', 'details', 'user_id',
    ];

    public function users(){

    	return $this->belongsToMany(User::class);
    }
}
